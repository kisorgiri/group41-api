
// database ==> container to hold data

// to perform data operation in database we need database management system

// Relational Database Management System
// Distributed database management system

// Relation database management system

// 1. Table based design
// LMS
// Books,Reviews,Users
// author,genre, price, distributor
// 2.each record are called row/ tuple
// 3.Schema based solution (each row must validate schema)
// 4.Relation between table exists
// 5.SQL database (Structured Query Language)
// 6.eg.. MySQL, Postgres, Sql-lite
// 7.non scalable


// Distributed Database Management system

// 1.Collection based design
// 2.each record are called document
// a document can be any valid json object
// 3.Schema less design 
// 4.relation doesnot exists
// 5. NO SQL = NOT only SQL
// 6.eg .. mongodb,dynamodb, redis
// 7. highly scalbale

// eg of books
// {
//  name:'hi',
//  genre:'programming',
//  reviews:[{
//    point:3,
//    message:'good',
//    user:'ram'
//  },{

//  },{

//  }] 
// }


// mongodb installation on windows

// c:/...programme files / mongodb ==> 4.3/bin 

// copy that path

// my computer
// right click==> propties advance==> environement variables
// system varibales==> path ==> edit
// and copied url in the path section and then save
// restart is requried

// use either gitbash or powershell

// mongod ==> driver initilization==> 27017 port mongodb server will be listening
// execute command mongo

// mongodb ---> distributed dbms 
// 

// to connect to mongod from command line
// use mongo command
// arrow head interface will apper once connection is successfully established

// once connection is established we are ready to execute shell command

// command

// show databases // list all the available databases in machine


// to select database

// use <db_name>
// if(existing db) select existing db
// else create new db with provided name and select it

// db ==> print selected database name
// show collections ==> list all the avaialble collections of selected database

// CORE DB Operations
// CRUD operations 
// Create, Read,Update and Delete

// Create

// db.<collection_name>.insert({valid json})
// db.<collection_name>.insertMany(array)
// NOTE => when creating document mongod itself adds _id property with ObjectId value

// Read
// db.<collection_name>.find({query_builder})
// db.<collection_name>.find({query}).pretty();
// db.<collection_name>.find({query}).count(); // returns document count matching given query
// db.<collection_name>.find({query}).sort({
  // _id:-1 // decending order with time of insertion
// }); 
// db.<collection_name>.find({query}).limit(<limit count>);
// db.<collection_name>.find({query}).skip(<skip count>);
// $exist ==> to check the existance of proeprty
// $gt/$gte greater then /greater then equals
// $lt/$lte less then /less then equals


// projection 
// either inclusion or exclusion
// syntax db.<collection_name>.find({query},{projection})

// $all
// $in 
// both are used to query from array of element
// $all ==> all provided array elemets must be matched
// $in ==> any element can be matched

// task is to find only one document from list of 20 documents whose position is 10th



// Update

// db.<collection_name>.update({},{},{})
// first object ==> query builder
// 2nd object ==> $set key and value will be another object which will be paylod to be updated
// 3rd object ==>optoions (optional)
// multi , upsert ...
// eg 
// db.<collection_name>.update({queyr},{$set:{payload}},{multi:true,upsert:true})

// Delete
// db.<collection_name>.remove({query})
// NOTE dont leave your query empty

// drop collection
// db.<collection_name>.drop()

// drop database;
// db.dropDatabase();



// ODM/ORM ==> Object Document Modelling / Object Relation mapping

// Relational DB ==> ORM
// DOcument ===> ODM

// Mongodb with nodejs ===> Mongoose ODM

// advantages

// schema based solution
// 
// indexing are lot for easier
// required,unique, ...
// data types
//ObjectId, Date

// middlewares
// pre
// methods
// db query methods

// DB backup and restore

// mongodump and mongorestore
// mongoexport and mongoimport

// NOTE ===> we can run above commands from any path 

// 1. BSON
// 2. Readable ==> JSON and CSV

// BSON
// backup
// command 
// mongodump

//from any path
// mongodump ===> creates a default dump folder with all available database backup
// mongodump --db  <selected_db_name>
// mongodump --db <db_name> --out <disired folder location>

// restore
// command ==> mongorestore
// mongorestore ==> restore from default dump folder
// mongorestore --drop ==> drop existing documents
// mongorestore <path_to_backup folder>


// JSON and CSV

// JSON
// mongoexport and mongoimport
// backup
// command ==> mongoexport

// mongoexport --db <db_name> --collection <collection_name> --query='{key:"value"}' --out <path_to_destination_with.json extension.
// mongoexport -d <db_name> -c <collection_name>  -o <path_to_destination_with.json extension.


// import 
// command ==> mongoimport 
// mongoimport --db <new or existing db> --collection <new or existing collection> <path_to_json file>


// CSV //
// backup
// mongoexport
// mongoexport --db <db_name> --collection <collection_name> --type=csv --fields 'comma separated propertyName to be exported' --out <path_to_destination_folder_with.csv extension>

// import
// command ==> mongoimport
// mongoimport --db <new or existing db> --collection <new or existing collection> --type=csv <path_to_csv_file> --headerline