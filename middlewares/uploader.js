const path = require('path');

const multer = require('multer');

// basic usage 
// const upload = multer({
//   dest: 'uploads/'
// })

function getStorageSettings(dirPath = 'images') {
  // check if directory exists
  // else create folder using appropritate fs methods

  return multer.diskStorage({
    filename: function (req, file, cb) {
      cb(null, Date.now() + '-' + file.originalname);
    },
    destination: function (req, file, cb) {
      cb(null, path.join(process.cwd(), 'uploads/' + dirPath + '/'))
      // cb(null, path.join(process.cwd(), `uploads/${dirPath}`))
    }
  })
}


// file filter
// umage filter

const imageFilter = function (req, file, cb) {
  let mimeType = file.mimetype;
  let content = mimeType.split('/')[0];
  if (content === 'image') {
    cb(null, true)
  } else {
    req.fileTypeErr = true;
    cb(null, false)
  }
}

const pdfFilter = function (req, file, cb) {
  let mimeType = file.mimetype;
  let content = mimeType.split('/')[1];
  if (content === 'pdf') {
    cb(null, true)
  } else {
    req.fileTypeErr = true;
    cb(null, false)
  }
}

const jsonFilter = function (req, file, cb) {
  let mimeType = file.mimetype;
  let content = mimeType.split('/')[1];
  if (content === 'json') {
    cb(null, true)
  } else {
    req.fileTypeErr = true;
    cb(null, false)
  }
}



const mapFilter = {
  image: imageFilter,
  pdf: pdfFilter,
  json: jsonFilter
}
// if doesnot exists

module.exports = function (filterType, directoryName) {
  const upload = multer({
    storage: getStorageSettings(directoryName),
    fileFilter: mapFilter[filterType]
  })
  return upload;
};

