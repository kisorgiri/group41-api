const jwt = require('jsonwebtoken');
const config = require('./../configs')
const UserModel = require('./../models/user.model')

module.exports = function (req, res, next) {
  // logic
  let token;
  if (req.headers['authorization'])
    token = req.headers['authorization'];
  if (req.headers['x-access-token'])
    token = req.headers['x-access-token']
  if (req.query.token)
    token = req.query.token;

  if (!token) {
    return next({
      msg: 'Authentication Failed! Token Not Provided',
      status: 400
    })
  }

  // in general 
  // token will be Bearer <space> token
  // remove bearer if available
  let finalToken = token.split(' ')[1];
  console.log('final token >>', finalToken)
  if (!finalToken) {
    return next({
      msg: 'Authentication Failed! Invalid Token',
      status: 400
    })
  }

  jwt.verify(finalToken, config.JWT_SECRET, function (err, done) {
    if (err) {
      return next(err);
    }
    console.log('token information>>', done)
    UserModel.findOne({
      _id: done._id
    }, function (err, user) {
      if (err) {
        return next(err);
      }
      if (!user) {
        return next({
          msg: 'User Removed From System',
          status: 404
        })
      }
      req.user = user;
      next();
    })
    // db operation
    // console.log('token verification successfull',done);
    // req.user = done
    // next();
  })

}

// possible errors
// 1st users session information is taken from token data

// eventhough user is removed from system token can be used for authentication

// solution 
// middleware should check for users in database

