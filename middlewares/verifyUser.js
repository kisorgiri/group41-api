module.exports = function (vendorId) {
  return function (req, res, next) {
    if (req.user._id === vendorId) {
      next()
    }
    else {
      next({
        msg: 'you dont right to take action for this product',
        status: 405
      })
    }
  }

}