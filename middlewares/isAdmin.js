module.exports = function (req, res, next) {
  if (req.user.role === 1) {
    next();
  } else {
    return next({
      msg: 'You dont have access',
      status: 403
    })
  }
}