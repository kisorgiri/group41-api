// remove logic here
const fs = require('fs');
const path = require('path');



function removeFile(path) {
  fs.unlink(path.join(process.cwd(), 'uploads/' + path, function (err, done) {
    if (err) {
      console.log('file remove failed', err)
    } else {
      console.log('file remove success')
    }
  }))
}

module.exports = removeFile;