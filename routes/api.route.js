const router = require('express').Router();
const authRouter = require('./../controllers/auth');
const userRouter = require('./../controllers/user');
const productRouter = require('./../modules/products/product.route')
const categoryRouter = require('./../modules/categories/categories.route')

// load middlewares
const authenticate = require('./../middlewares/authentication');

router.use('/auth', authRouter)
router.use('/users', authenticate, userRouter)
router.use('/products', productRouter)
router.use('/category', categoryRouter)


module.exports = router;