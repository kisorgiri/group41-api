const ProductQuery = require('./product.query')

function fetchProduct(req, res, next) {
    const condition = {};
    // when not admin
    if (req.user.role !== 1) {
        condition.vendor = req.user._id;
    }
    ProductQuery.fetch(condition)
        .then(function (products) {
            res.json(products)
        })
        .catch(function (err) {
            next(err)
        })
    // result handle
    // success res.json
    // failure ==> next()
}

function fetchById(req, res, next) {
    const condition = { _id: req.params.id };
    ProductQuery.fetch(condition)
        .then(function (products) {
            res.json(products[0])
        })
        .catch(function (err) {
            next(err)
        })
}


function insertProduct(req, res, next) {
    // 
    console.log('req.files >>>', req.files);
    const data = req.body;
    if (data.tags)
        data.tags = data.tags.split(',');
    // add images in data
    // add vendror in data
    data.vendor = req.user._id;
    if (req.files && req.files.length) {
        data.images = req.files.map(function (item) {
            return item.filename;
        })
    }

    ProductQuery
        .insert(data)
        .then(function (result) {
            res.json(result)
        })
        .catch(function (err) {
            next(err)
        });
}

function searchProduct(req, res, next) {
    const searchCondition = {};
    // TODO create searchCondition
    if (req.body.category) {
        searchCondition.category = req.body.category
    }
    if (req.body.name) {
        searchCondition.name = req.body.name
    }
    if (req.body.minPrice) {
        searchCondition.price = {
            $gte: req.body.minPrice
        }
    }
    if (req.body.maxPrice) {
        searchCondition.price = {
            $lte: req.body.maxPrice
        }
    }
    if (req.body.minPrice && req.body.maxPrice) {
        searchCondition.price = {
            $gte: req.body.minPrice,
            $lte: req.body.maxPrice
        }
    }
    if (req.body.fromDate && req.body.toDate) {
        const fromDate = new Date(req.body.fromDate).setHours(0, 0, 0, 0); // 
        const toDate = new Date(req.body.toDate).setHours(23, 59, 59, 999);

        searchCondition.createdAt = {
            $gte: new Date(fromDate),
            $lte: new Date(toDate)
        }
    }

    if (req.body.tags) {
        let tags = req.body.tags.split(',') // into array
        searchCondition.tags = {
            $in: tags
        }
    }
    console.log('search condition', searchCondition)
    ProductQuery.fetch(searchCondition)
        .then(function (products) {
            res.json(products)
        })
        .catch(function (err) {
            next(err)
        })
}

function update(req, res, next) {
    console.log('images >>', req.files)
    const data = req.body;
    data.vendor = req.user._id

    if (req.files && req.files.length) {
        data.newImages = req.files.map(function (item) {
            return item.filename;
        })
    }
    delete data.images;
    // data preparation
    ProductQuery
        .update(req.params.id, data)
        .then(function (result) {
            res.json(result)
        })
        .catch(function (err) {
            next(err)
        });

}

function remove(req, res, next) {
    ProductQuery.remove(req.params.id, req.user._id)
        .then(function (result) {
            if (!result) {
                return next({
                    msg: 'Product Not Found',
                    status: 404
                })
            }
            res.json(result)
        })
        .catch(function (err) {
            next(err)
        });
}

function addReview(req, res, next) {
    req.body.user = req.user._id;
    ProductQuery
        .addReview(req.params.productId, req.body)
        .then(function (result) {
            res.json(result)
        })
        .catch(function (err) {
            next(err)
        });

}

module.exports = {
    fetchProduct,
    fetchById,
    searchProduct,
    update,
    remove,
    insertProduct,
    addReview
}