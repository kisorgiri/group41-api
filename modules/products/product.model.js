const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const reviewSchema = new Schema({
    point: {
        type: Number,
        min: 1,
        max: 5
    },
    message: String,
    user: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    }
}, {
    timestamps: true
})

const productSchema = new Schema({
    // 
    name: {
        type: String,
        required: true
    },
    modelNo: String,
    price: Number,
    color: String,
    description: String,
    quantity: Number,
    images: [String],
    size: String,
    purchasedDate: Date,
    salesDate: Date,
    manuDate: Date,
    expiryDate: Date,
    weight: String,
    slug: String,
    vendor: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    },
    category: {
        type: Schema.Types.ObjectId,
        ref: 'categories'
    },
    // categories:String
    quality: String,
    reviews: [reviewSchema],
    isFeatured: Boolean,
    isReturnEligible: Boolean,
    isIsoCertified: Boolean,
    status: {
        type: String,
        enum: ['booked', 'available', 'out of stock'],
        default: 'available'
    },
    brand: String,
    description_summary: String,
    specifications: String,
    discount: {
        discountedItem: Boolean,
        discountType: {
            type: String,
            enum: ['percentage', 'qunatity', 'value'],
        },
        discountValue: String
    },
    warrentyStatus: Boolean,
    warrentyPeriod: String,
    tags: [String]

}, {
    timestamps: true
})

module.exports = mongoose.model('product', productSchema);