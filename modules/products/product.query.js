const ProductModel = require('./product.model');

function mapProductReq(productData, product) {
    if (productData.name)
        product.name = productData.name;
    if (productData.modelNo)
        product.modelNo = productData.modelNo;
    if (productData.price)
        product.price = productData.price;
    if (productData.color)
        product.color = productData.color;
    if (productData.description)
        product.description = productData.description;
    if (productData.quantity)
        product.quantity = productData.quantity;
    if (productData.images)
        product.images = productData.images;
    if (productData.category)
        product.category = productData.category;
    if (productData.size)
        product.size = productData.size;
    if (productData.purchasedDate)
        product.purchasedDate = productData.purchasedDate;
    if (productData.salesDate)
        product.salesDate = productData.salesDate;
    if (productData.manuDate)
        product.manuDate = productData.manuDate;
    if (productData.expiryDate)
        product.expiryDate = productData.expiryDate;
    if (productData.weight)
        product.weight = productData.weight;
    if (productData.slug)
        product.slug = productData.slug;
    if (productData.vendor)
        product.vendor = productData.vendor;
    if (productData.quality)
        product.quality = productData.quality;
    if (productData.isFeatured)
        product.isFeatured = productData.isFeatured;
    if (productData.isReturnEligible)
        product.isReturnEligible = productData.isReturnEligible;
    if (productData.isIsoCertified)
        product.isIsoCertified = productData.isIsoCertified;
    if (productData.status)
        product.status = productData.status;
    if (productData.brand)
        product.brand = productData.brand;
    if (productData.description_summary)
        product.description_summary = productData.description_summary;
    if (productData.specifications)
        product.specifications = productData.specifications;
    if (productData.warrentyStatus)
        product.warrentyStatus = productData.warrentyStatus;
    if (productData.warrentyPeriod)
        product.warrentyPeriod = productData.warrentyPeriod;
    if (productData.tags)
        product.tags = productData.tags;
    if (!product.discount)
        product.discount = {};
    if (productData.discountedItem)
        product.discount.discountedItem = true;
    if (productData.setDiscountedItemFalse)
        product.discount.discountedItem = false;
    if (productData.discountType)
        product.discount.discountType = productData.discountType;
    if (productData.discountValue)
        product.discount.discountValue = productData.discountValue;
    return product;
}

/**
 * 
 * @param {Object} condition 
 */
function fetch(condition) {
    console.log('condition is >', condition)
    // return new Promise(function (resolve, reject) {
    //     ProductModel
    //         .find(condition)
    //         .sort({
    //             _id: -1
    //         })
    //         .exec(function (err, products) {
    //             if (err) {
    //                 return reject(err);
    //             }
    //             resolve(products)
    //         });
    // })

    return ProductModel
        .find(condition)
        .sort({
            _id: -1
        })
        .populate('vendor', {
            username: 1
        })
        .populate('category')
        .populate('reviews.user', { email: 1 })
        .exec();

    // 
}

/**
 * 
 * @param {Object} data 
 */
function insert(data) {
    const newProduct = new ProductModel({});
    const mappedProduct = mapProductReq(data, newProduct);
    return mappedProduct.save();
}

/**
 * 
 * @param {String} id 
 * @param {Object} data 
 * @returns Promise
 */
function update(id, data) {
    console.log('data is >>', data)
    // filesTORemove will be string
    const filesToRemove = data.filesToRemove ? data.filesToRemove.split(',') : [];
    // remove name  from database
    // remove actual image from server

    return new Promise(function (resolve, reject) {
        ProductModel.findById(id, function (err, product) {
            if (err) {
                return reject(err)
            }
            if (!product) {
                return reject({
                    msg: 'Product Not Found',
                    status: 404
                })
            }
            if (product.vendor.toString() != data.vendor.toString()) {
                return reject({
                    msg: 'You dont have access to take action for this product',
                    status: 403
                })
            }

            const updatedExistingImages = existingImages.filter((item, index) => {
                return !filesToRemove.includes(item);
            })
            const finalImages = [
                ...(data.newImages ? data.newImages : []),
                ...updatedExistingImages
            ]

            const updatedProduct = mapProductReq(data, product)
            updatedProduct.images = finalImages;
            updatedProduct.save(function (err, updated) {
                if (err) {
                    return reject(err);
                }
                resolve(updated)
            })
        })

    })

}

function remove(id, userId) {
    return new Promise(function (resolve, reject) {
        ProductModel.findById(id, function (err, product) {
            if (err) {
                return reject(err)
            }
            if (!product) {
                return reject({
                    msg: 'Product Not Found',
                    status: 404
                })
            }
            if (product.vendor.toString() !== userId.toString()) {
                return reject({
                    msg: 'You dont have access to take action for this product',
                    status: 403
                })
            }
            // product found now remove

            product.remove(function (err, removed) {
                if (err) {
                    return reject(err)
                }
                resolve(removed)
            })

        })

    })
}

function addReview(productId, reviewData) {
    return new Promise(function (resolve, reject) {
        ProductModel.findById(productId, function (err, product) {
            if (err) {
                return reject(err)
            }
            if (!product) {
                return reject({
                    msg: 'Product Not Found',
                    status: 404
                })
            }

            let finalReview = {};

            if (reviewData.reviewPoint)
                finalReview.point = reviewData.reviewPoint;
            if (reviewData.reviewMessage)
                finalReview.message = reviewData.reviewMessage;
            if (reviewData.user)
                finalReview.user = reviewData.user;

            product.reviews.push(finalReview);

            product.save(function (err, saved) {
                if (err) {
                    return reject(err)
                }
                resolve(saved)
            })

        })
    })


}


// object shorthand
module.exports = {
    fetch,
    insert,
    update,
    remove,
    addReview
}