const router = require('express').Router();
const ProductCtrl = require('./product.controller');
const Uploader = require('./../../middlewares/uploader')('image')
const Authenticate = require('./../../middlewares/authentication')
const isAdmin = require('./../../middlewares/isAdmin')

router.route('/')
  .get(Authenticate, ProductCtrl.fetchProduct)
  .post(Authenticate, Uploader.array('images'), ProductCtrl.insertProduct);

router.route('/review/:productId')
  .post(Authenticate, ProductCtrl.addReview)

router.route('/search')
  .get(ProductCtrl.searchProduct)
  .post(ProductCtrl.searchProduct)

router.route('/:id')
  .get(Authenticate, ProductCtrl.fetchById)
  .put(Authenticate, Uploader.array('images'), ProductCtrl.update)
  .delete(Authenticate, ProductCtrl.remove);

module.exports = router;