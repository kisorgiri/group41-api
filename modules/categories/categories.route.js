const router = require('express').Router();
const authentication = require('../../middlewares/authentication');
const categoriesCtrl = require('./categories.controller')

router.get('/', authentication, categoriesCtrl.fetch);
router.post('/search', categoriesCtrl.search);

module.exports = router;