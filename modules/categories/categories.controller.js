const categoriesQuery = require('./categories.query');
const fetch = (req, res, next) => {
  const condition = {};
  // shortcut >>> db entry for all categories
  // find by vendor id
  // to find by vendor we must insert categoes with vendor id
  categoriesQuery
    .find(condition)
    .then(result => {
      res.json(result);
    })
    .catch(err => next(err));

}


const search = (req, res, next) => {
  const searchCondition = {}; // default condition
  categoriesQuery
    .find(searchCondition)
    .then(result => {
      res.json(result);
    })
    .catch(err => next(err));

}

module.exports = {
  fetch,
  search
}