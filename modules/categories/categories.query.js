const CategoryModel = require('./categories.model');

const find = (condition) => {
  return CategoryModel.find(condition);
}

module.exports = {
  find
}