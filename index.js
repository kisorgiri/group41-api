const express = require('express');
const config = require('./configs')
const PORT = config.PORT;
const morgan = require('morgan');
const path = require('path');
const cors = require('cors');

// global keyword
// process object
// __dirname 
// __filename 
// global object


// import routing level middleware
const apiRouter = require('./routes/api.route')

require('./db_init'); //part of index file db_init file will be executed


const app = express();
// app is entire express framework


// load third party middleware
app.use(morgan('dev'))
app.use(cors()); // enable all request

// inbuilt middleware  for serving static file
// app.use(express.static('uploads'))
app.use('/file', express.static(path.join(process.cwd(), 'uploads')))

// incoming data must be parsed by server according to their content type
// parser for x-www-form-url-encoded
app.use(express.urlencoded({
  extended: true
}))
app.use(express.json())
// this middleware will add  a body property with parsed value
// config routing level middleware
// app.use(authRouter) //[without path] regardless of http url and method control will be passed to auth router
app.use('/api', apiRouter)

// 404 error catch block
app.use(function (req, res, next) {
  // res.json({
  //   msg: 'Not Found',
  //   status: 404
  // })
  next({
    msg: 'Not Found',
    status: 404
  })
})

// middleware with 4 argument is error handling middleware
// error handling middleware don't came into action between req-res cycle
// error handling middleware must be called
// next with argument will invoke error handling middleware
// what ever is sent in next call it will be passed to 1st argument placeholder in error handling middleware
app.use(function (err, req, res, next) {
  console.log('at error handling middleware', err)
  // 1st argument is for error
  // 2nd argument http request object
  // 3rd argument http response object
  // 4th argument for next middleware function reference
  // TODO set status code for error response
  res.status(err.status || 400)
  res.json({
    msg: err.msg || err,
    status: err.status || 400
  })
})


app.listen(PORT, function (err, done) {
  if (err) {
    console.log('error >>', err)
  } else {
    console.log('server listening at port ' + PORT)
  }
})



// documentation
// middleware
// 
// function that came into action in between req-res cycle
// middleware can access http request object
// http response object 
// and next middleware function reference

// it can modify req-res object
// NOTE ===> the order of middleware function is very important

// syntax

// function(req,res,next){
// req or 1st arg placeholder is for http request object
// res or 2nd arg placeholder is for http response object
// next or 3rd arg placholder is for next middleware function reference
// }

// configuration
// we can use http verb and use all method to configure middleware
// app.use(),app.get(),app.all

// types of middleware

// 1. application level middleware
// 2, routing level middleware
// 3. third party middleware
// 4. inbuilt middleware
// 5. error handling middleware

// application level middleware

// middlewares with direct scope of req,res and next are application level middleware


// incoming data must be parsed