const mongoose = require('mongoose');
const schema = mongoose.Schema;

const userSchema = new schema({
  // db modelling
  name: String,
  username: {
    type: String,
    required: true,
    unique: true,
    lowercase: true
  },
  password: {
    type: String,
    required: true
  },
  email: {
    type: String,
    unique: true,
    sparse: true
  },
  contactNumber: {
    type: Number
  },
  gender: {
    type: String,
    enum: ['male', 'female', 'other']
  },
  address: {
    temporaryAddress: [String],
    permanentAddress: String
  },
  dob: {
    type: Date
  },
  role: {
    type: Number, //1 for admin, 2 for normal user
    default: 2
  },
  status: {
    type: String,
    default: 'active'
  },
  image: {
    type: String
  },
  passwordResetExpiry: Date,
  passwordResetToken: String

}, {
  timestamps: true
})

const userModel = mongoose.model('user', userSchema)
module.exports = userModel;

// review schema

// notification schema

// message schema