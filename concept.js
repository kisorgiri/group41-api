// JS ==> 1995
// 2009 ==> server side JS 
// runtime==> NODE JS by ryan dahl

// server side application area
// network
// real time server 
// API

// web

// web server
// nodejs ==> express 

// web client
// react

// http protocol
// protocol ==> set of rules to communicate
// smtp 
// ftp
// queue protocol
// http 
// http verb (methods)
// status code 100,200,300,400,500
// 

// node --version ==> check node version

// npm and npx are pacakge management tool installed during node installation
// 

// npm init ==> it will initilize javascript project
// it will create a package.json file

// pacakge.json file is project introductory file
// it will hold overall information about project
// try to maintain updated information inside pacakge.json


// npmjs.com ==>
// global container to hold javascript related pacakges
// npm registry ==> pacakges 

// to install pacakges from npmjs to local project folder

// command
// npm install <pacakge-name>

// after installation
// pacakge-lock.json file is added and node_modules folder is added

// node_modules ==> folder to keep installed pacakges

// pacakge-lock.json ==> maintains exact version of pacakges during time of installation

// npm install ==> install all the pacakges listed in dependecies section

// npm install --save-dev ==> install pacakges and store installed pacakge inforamtion iside devDependecies section of pacakge.json


// remove packages
// npm uninstall <package-name>


// module => module  communication
// model ==> db term

// 2 file is necessary
// 1 file will export and another will import

// web application

// client server architecture

// server ==> programme that will respond to client request

// client ==>programme that initiate request

// node/express ==> server

// react ===> client

// web 
// MVC is commonly used architecture to develop web application
// Model ==> database  Mongodb (Express)
// View ==> Views ==> React
// controller ==> busic logic (Express)

// API ==> Application programming interface

// HTTP ==> 
// web client == http client
// web server ==> http server


// E-Commerce applications
// Entities

// user 
// Product
// attributes of product

// categories
// attributes of cateogies

// Notifications
// reviews
// carts
// payment_gateway eg.. esew, khalti information
// payment_history ==> amt, reason , gateway,id 



// token based authentication

// authentication vs authorization

// authentication ==> identification
// username, password
// session-cookies ==>  session is stored in server sesison_id is sent to client in form of cookies
// and in every http call session id from cookies is sent from browser ==> server ma session id ko through authentication

// token based authentication
// username, password
// server ===> token generation ==> generated token is not stored in server ==> sent to client
// client can be , web, mobile, desktop, server
// token should stored by clinet 
// whoever client has valid token it is authenticated by server
// Bearer token 



// questions
// 1. before record is created in db file is uploaded in server
// so if db operation failed file will be there in server



// 2. when deleting the image is not removed

// 3. directory is not created automatically
// 


// var categories = 


// REST API

// API (Application programming interface)
// HTTP Protocol
// API==> Combination of http method and url
// /login POST
// /register POST
// /payment POST


// REST ==>Representational State Transfer 
// following points needs to be addressed to be in REST archiecture

// 1. Stateless
// 2. Correct use of http verb
  // GET ==> Data Fetch
  // POST ==> Data send
  // PUT/PATCH ==> Data Update
  // DELETE ==> Remove
// 3. Data format can be either JSON or XML

// NIT ==> 
// url name should be noun instead of verb
// updated delete
// Caching ==> db call can be cached (GET )
// resource name can be separated by - hyphen


// TODO 
// server side remaining 
// Email Send logic 
// web sockets other than http 
// ==> http server <--> http client
// database ---> aggregation
// deployement
// db -==> db cloud service provider
// api ==> platform ==> aws
