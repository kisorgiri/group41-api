const mongodb = require('mongodb');
const mongoClient = mongodb.MongoClient;
const conxnURL = 'mongodb://localhost:27017';
const dbName = 'group41db';
const oid = mongodb.ObjectId;

module.exports = {
  mongoClient,
  conxnURL,
  dbName,
  oid
}
// object shorthand
