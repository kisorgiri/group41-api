const http = require('http');
const fileOp = require('./file');
// http ==> 
// rule ==> endpoiint==> combination of http method and url

// method ==> GET,POST,PUT, DELETE,
// url can be anything
const server = http.createServer(function (req, res) {
  console.log('client connected to server')
  // req or 1st arg placeholder is for http request object
  // res or 2nd arg placeholder is for http response object
  console.log('req.method >>', req.method);
  console.log('req.url >>', req.url)// string
  var urlIntoArr = req.url.split('/');
  // split('/')


  if (req.url === '/write') {
    // fileOp.write
    // result should be sent to requesting client
    fileOp.w('kishor.abcd', 'how are you?')
      .then(function (data) {
        res.end('succeess in write', data)
      })
      .catch(function (err) {
        res.end('failure in write ' + err)
      })
  } else if (req.url === "/read") {
    // read
  } else {
    res.end('nothing to perform')

  }
  // req-res cycle must be completed
  // regardless of http method and http url this callback is executed

  // http req- res cycle must be completed
  // req headers
  // req.method
  // req url
  // 
  // response must be sent
  // use 2nd arg 
  // use method end to complete req-res cycle

  // developers role ==> req ===> 
  // parse request
  // validate request
  // process
  // db operation
  // send appropriate response

})



// programme ==> set of instruction

// to run a programme as a process we need PORT and host(optional);

server.listen(9090, 'localhost', function (err, done) {
  if (err) {
    console.log('error in listening >', err)
  } else {
    console.log('server listening at port 9090')
    console.log('press CTRL + C to exit')
  }
});

