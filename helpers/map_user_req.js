module.exports = function (userData, user) {

  if (userData.name)
    user.name = userData.name;
  if (userData.username)
    user.username = userData.username;
  if (userData.password)
    user.password = userData.password;
  if (userData.email)
    user.email = userData.email;
  if (userData.contactNumber)
    user.contactNumber = userData.contactNumber
  if (userData.gender)
    user.gender = userData.gender;
  if (userData.dob)
    user.dob = userData.dob;
  if (userData.role)
    user.role = userData.role;
  if (userData.status)
    user.status = userData.status;
  if (!user.address)
    user.address = {};
  if (userData.temporaryAddress)
    user.address.temporaryAddress = typeof (userData.temporaryAddress) === 'string' ? userData.temporaryAddress.split(',') : userData.temporaryAddress;
  if (userData.permanentAddress)
    user.address.permanentAddress = userData.permanentAddress;
  if (userData.image)
    user.image = userData.image;
  return user;
}