const router = require('express').Router();

const UserModel = require('./../models/user.model');
const mapUserData = require('./../helpers/map_user_req');
const uploader = require('./../middlewares/uploader')('image')
const isAdmin = require('./../middlewares/isAdmin')
const fs = require('fs');
const path = require('path')

router.route('/')
  .get(function (req, res, next) {
    const condition = {};
    UserModel
      .find(condition)
      .limit(2)
      .skip(1)
      .sort({
        _id: -1
      })
      .exec(function (err, users) {
        if (err) {
          return next(err);
        }
        res.json(users)
      })
  });

router.route('/search')
  .get(function (req, res, next) {
    res.send('hi from user search')
  })
  .post(function (req, res, next) {

  });

router.route('/:id')
  .get(function (req, res, next) {
    UserModel.findById(req.params.id, function (err, user) {
      if (err) {
        return next(err);
      }
      res.json(user)
    })
  })
  .put(uploader.single('image'), function (req, res, next) {
    console.log('req.file >>', req.file)
    if (req.fileTypeErr) {
      return next({
        msg: 'Invalid File Format',
        status: 400
      })
    }

    UserModel.findById(req.params.id, function (err, user) {
      if (err) {
        return next(err);
      }
      if (!user) {
        return next({
          msg: 'User Not Found',
          status: 404
        })
      }
      var oldImage = user.image;
      // user found now update
      // user is mongoose object
      if (req.file) {
        req.body.image = req.file.filename;
      }
      const updatedMappedUser = mapUserData(req.body, user);


      updatedMappedUser.save(function (err, updated) {
        if (err) {
          return next(err);
        }
        // if req.file exists remove old images once update is complete
        if (req.file) {
          // remove call
          // fs.unlink(path.join(process.cwd(), 'uploads/images/' + oldImage), function (err, done) {
          //   if (!err) {
          //     console.log('file removed')
          //   }
          // })
        }
        res.json(updated)
      })
    })
  })
  .delete(isAdmin, function (req, res, next) {

    UserModel.findByIdAndRemove(req.params.id, function (err, removed) {
      if (err) {
        return next(err)
      }
      if (!removed) {
        return next({
          msg: 'User Not Found',
          status: 400
        })
      }
      res.json(removed)
      // remove image from server

    })

  });




module.exports = router;


// NOTE the order of middleware is very important