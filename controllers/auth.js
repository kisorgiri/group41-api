// subroute
const express = require('express');
const router = express.Router();
const UserModel = require('./../models/user.model');
const mapUserReq = require('./../helpers/map_user_req')
const uploader = require('./../middlewares/uploader')('image', 'images')
const passwordHash = require('password-hash');
const jwt = require('jsonwebtoken');
const config = require('./../configs')
const removeFile = require('./../utils/removeFIle');
const nodemailer = require('nodemailer');

// test account gmail
const sender = nodemailer.createTransport({
  service: 'Gmail',
  auth: {
    user: 'broadwaytest44@gmail.com',
    pass: 'Broadwaytest44!'
  }
})

function prepareEmail(data) {
  return {
    from: 'Group 41 Support Team', // sender address
    to: data.email, // list of receivers comma separated value
    subject: "Forgot Password ✔", // Subject line
    text: "Forgot Password", // plain text body
    html: `
    <p>Hi <strong>${data.name}</strong>,</p>
    <p>We noticed that you are having trouble logging into our system please click link below to reset your password</p>
    <p><a href="${data.link}">click here to reset password</a></p>
    <p>Regards,</p>
    <p>Group 41 Support Team</p>
    <p>2022</p>
    `, // html body
  }
}

const mongodb = require('mongodb');
const mongoClient = mongodb.MongoClient;

function generateToken(data) {
  return jwt.sign({
    _id: data._id,
    role: data.role,
    username: data.username
  }, config.JWT_SECRET)
}

router.get('/province', function (req, res, next) {
  mongoClient.connect('mongodb://localhost:27017', function (err, client) {
    if (err) {
      return next(err);
    }
    const db = client.db('group41db');
    db.collection('provinces').find({})
      .toArray(function (err, data) {
        if (err) {
          return next(err);
        }
        res.json(data)
      })
  })
})

// console.log('root directory >>', process.cwd())
// console.log('__dirname >>', __dirname)
// router is routing level middleware

router.post('/login', function (req, res, next) {
  // data from form
  UserModel.findOne({
    $or: [{
      username: req.body.username
    },
    {
      email: req.body.username
    }]
  })
    .then(function (user) {
      if (!user) {
        return next({
          msg: 'Invalid Username',
          status: 404
        })
      }
      if (user.status === 'inactive') {
        return next({
          msg: 'Your Account is disabled please contact system administrator for support',
          status: 400
        })
      }
      let isMatched = passwordHash.verify(req.body.password, user.password)
      if (!isMatched) {
        return next({
          msg: 'Invalid Password',
          status: 404
        })
      }

      let token = generateToken(user);
      res.json({
        user: user,
        token: token
      })
      // 
      // password verification
      // token generation
      // send appropriate response
      // res.json(user)
    })
    .catch(function (err) {
      next(err)
    })
})

router.post('/register', uploader.single('image'), function (req, res, next) {
  console.log('reqbody.>>', req.body);
  console.log('req.file >>', req.file)
  if (req.fileTypeErr) {
    return next({
      msg: 'Invalid File Format',
      status: 400
    })
  }
  // data validation
  // password must be 8 characters long
  // username must have a uppercase letter
  // TODO add validation layer using joi or express-validator
  const newUser = new UserModel({}); // mongoose object
  // newuser is mongoose object
  // it will have certain properties and methods

  // var obj = {addr:'hello'};
  // obj.name = 'hji';
  if (req.file)
    req.body.image = req.file.filename
  const newMappedUser = mapUserReq(req.body, newUser)
  newUser.password = passwordHash.generate(req.body.password);
  newUser.save(function (err, done) {
    if (err) {
      // remove here
      if (req.file) {
        removeFile('/images/' + req.file.filename)
      }
      return next(err);
    }
    res.json(done);
  })


})

router.post('/forgot-password', function (req, res, next) {
  UserModel.findOne({
    email: req.body.email
  })
    .exec(function (err, user) {
      if (err) {
        return next(err)
      }
      if (!user) {
        return next({
          msg: 'Email not registered yet',
          status: 404
        })
      }
      // email stuff
      let emailData = {
        name: user.username,
        email: user.email,
        link: req.headers.origin + '/reset_password/' + user._id
      }
      let mailContent = prepareEmail(emailData);

      let expiryTime = Date.now() + (1000 * 60 * 60 * 24 * 2);
      user.passwordResetExpiry = expiryTime;
      user.save(function (err, done) {
        if (err) {
          return next(err);
        }
        sender.sendMail(mailContent, function (err, done) {
          if (err) {
            return next(err)
          }
          res.json(done)
        })
      })

    })
})

router.post('/reset-password/:id', function (req, res, next) {
  UserModel
    .findOne({
      _id: req.params.id,
      passwordResetExpiry: {
        $gte: Date.now()
      }
    })
    .then(function (user) {
      if (!user) {
        return next({
          msg: 'Invalid password reset link',
          status: 404
        })
      }
      // check expiry time
      user.password = passwordHash.generate(req.body.password);
      user.passwordResetExpiry = null;
      user.save(function (err, done) {
        if (err) {
          return next(err);
        }
        res.json(done)
      })
    })
    .catch(function (err) {
      next(err)
    })

})

module.exports = router;