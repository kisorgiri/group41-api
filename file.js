// es5
const fs = require('fs');

// create a file with content
// fs.writeFile('./tests/random1.txt', 'welcome to nodejs and server side programming', function (err, done) {
//   if (err) {
//     console.log('error in writing file', err);
//   }
//   else {
//     console.log('success in writing file', done)
//   }
// });

// create my own function to write

function myWrite(fileName, content) {
  return new Promise(function (resolve, reject) {
    fs.writeFile('./tests/' + fileName, content, function (err, done) {
      if (err) {
        reject(err)
      } else {
        resolve(done)
      }
    })
  })

}

// myWrite('kishor.txt', 'i am happy')
//   .then(function (data) {
//     console.log("success in my write >>", data)
//   })
//   .catch(function (err) {
//     console.log('error is >', err)
//   })

module.exports = {
  w: myWrite
}


// task is to continue with 
// read,rename and remove

// read

// fs.readFile('./tests/kishor.txt', 'UTF-8',function (err, done) {
//   if (err) {
//     console.log('error in reading >>', err)
//   }
//   else {
//     console.log('success in reading', done)
//   }
// })

// rename
// fs.rename('oldpath','newpath',callback)

// remove
// fs.unlink('path to file',callback)

// task for read ,rename and remove
// create your own function to read with arg
// use callback or promise to handle result of your own function
// export from one file and import in another file to execute

// MVC architecture
// Client-Server communication
// Server creation